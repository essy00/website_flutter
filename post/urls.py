from django.urls import path
from .views import *

app_name = 'post'

urlpatterns = [
    path('index/', post_index, name='index'),
    path('detail/<str:slug>', post_detail, name='detail'),
    path('create/', post_create, name='create'),
    path('update/<str:slug>', post_update, name='update'),
    path('delete/<str:slug>', post_delete, name='delete'),
]
