from django.shortcuts import render, get_object_or_404, HttpResponseRedirect, redirect, Http404
from .models import Post
from .forms import PostForm
from django.contrib import messages
from django.utils.text import slugify
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def post_index(request):
    post_list = Post.objects.all()
    paginator = Paginator(post_list, 5)

    page_number = request.GET.get('page')
    posts = paginator.get_page(page_number)
    return render(request, 'post/posts.html', {'posts': posts})


def post_detail(request, slug):
    post = get_object_or_404(Post, slug=slug)
    context = {
        'post': post
    }

    return render(request, 'post/detail.html', context)


def post_create(request):
    if request.user.is_superuser:
        form = PostForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            post = form.save()
            return HttpResponseRedirect(post.get_absolute_url())

        context = {
            'form': form
        }

        return render(request, 'post/form.html', context)

    else:
        raise Http404()


def post_update(request, slug):
    if request.user.is_superuser:
        post = get_object_or_404(Post, slug=slug)
        form = PostForm(request.POST or None, request.FILES or None, instance=post)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(post.get_absolute_url())

        context = {
            'form': form,
        }

        return render(request, 'post/form.html', context)

    else:
        raise Http404()


def post_delete(request, slug):
    if request.user.is_superuser:
        post = get_object_or_404(Post, slug=slug)
        post.delete()
        return redirect('post:index')

    else:
        raise Http404()
